# Registro y control de beneficiarios

Creado por dM

## Versiones
```
Django==1.8.8
Python==2.7
```
## Comandos usados en secuencia para probar el proyecto

$ python manage.py makemigrations registro

$ python manage.py migrate

$ python manage.py createsuperuser

$ python manage.py migrate

$ python manage.py runserver
