# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from registro.models import Persona, Direccion
from forms import PersonaForm, DireccionForm
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages


#########################
##### Beneficiarios #####
#########################

class Consultar_Beneficiarios(ListView):
    model = Persona


class Registrar_Beneficiarios(SuccessMessageMixin,CreateView):
    model = Persona
    form_class = PersonaForm
    success_url = reverse_lazy('registro:consultar_beneficiarios')
    success_message = "Se registro el beneficiario con éxito"


class Editar_Beneficiarios(SuccessMessageMixin,UpdateView):
    model = Persona
    form_class = PersonaForm
    success_url = reverse_lazy('registro:consultar_beneficiarios')
    success_message = "Se actualizo el beneficiario con éxito"


class Borrar_Beneficiarios(SuccessMessageMixin,DeleteView):
    model = Persona
    success_url = reverse_lazy('registro:consultar_beneficiarios')
    success_message = "Se elimino el beneficiario con éxito"

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(Borrar_Beneficiarios, self).delete(request, *args, **kwargs)


#######################
##### Direcciones #####
#######################

class Consultar_Direcciones(ListView):
    model = Direccion


class Registrar_Direcciones(SuccessMessageMixin,CreateView):
    model = Direccion
    form_class = DireccionForm
    success_url = reverse_lazy('registro:consultar_direcciones')
    success_message = "Se registro la dirección con éxito"


class Editar_Direcciones(SuccessMessageMixin,UpdateView):
    model = Direccion
    form_class = DireccionForm
    success_url = reverse_lazy('registro:consultar_direcciones')
    success_message = "Se actualizo la dirección con éxito"


class Borrar_Direcciones(SuccessMessageMixin,DeleteView):
    model = Direccion
    success_url = reverse_lazy('registro:consultar_direcciones')
    success_message = "Se elimino la dirección con éxito"

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(Borrar_Direcciones, self).delete(request, *args, **kwargs)
