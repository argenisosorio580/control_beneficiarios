# -*- coding: utf-8 -*-
from django.contrib import admin
from registro.models import Persona, Direccion

admin.site.register(Persona)
admin.site.register(Direccion)
