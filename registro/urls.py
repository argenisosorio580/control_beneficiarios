# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from registro import views


urlpatterns = patterns('',
	##### Beneficiarios #####
    url(r'^$', views.Consultar_Beneficiarios.as_view(), name='consultar_beneficiarios'),
    url(r'^registrar_beneficiarios$', views.Registrar_Beneficiarios.as_view(), name='registrar_beneficiarios'),
    url(r'^editar_eneficiarios/(?P<pk>\d+)$', views.Editar_Beneficiarios.as_view(), name='editar_beneficiarios'),
    url(r'^borrar_beneficiarios/(?P<pk>\d+)$', views.Borrar_Beneficiarios.as_view(), name='borrar_beneficiarios'),
    ##### Direcciones #####
    url(r'^consultar_direcciones$', views.Consultar_Direcciones.as_view(), name='consultar_direcciones'),
    url(r'^registrar_direcciones$', views.Registrar_Direcciones.as_view(), name='registrar_direcciones'),
    url(r'^editar_direcciones/(?P<pk>\d+)$', views.Editar_Direcciones.as_view(), name='editar_direcciones'),
    url(r'^borrar_direcciones/(?P<pk>\d+)$', views.Borrar_Direcciones.as_view(), name='borrar_direcciones'),
)
