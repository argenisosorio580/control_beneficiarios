# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from datetime import datetime


class Direccion(models.Model):
    nombre_direccion = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nombre_direccion

    def get_absolute_url(self):
        return reverse('registro:editar_direcciones', kwargs={'pk': self.pk})


class Persona(models.Model):
    codigo = models.CharField(max_length=200)
    nombres = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    cedula = models.CharField(max_length=12)
    telefono = models.CharField(max_length=200)
    estado = models.CharField(max_length=30)
    municipio = models.CharField(max_length=30)
    parroquia = models.CharField(max_length=30)
    direccion = models.ForeignKey(Direccion)
    observacion = models.TextField(max_length=1000)
    enviado_a = models.CharField(max_length=200)
    estatus = models.CharField(max_length=1)
    fecha_solicitud = models.DateField(default=datetime.now)
    fecha_respuesta = models.DateField(blank=True,null=True)

    def __unicode__(self):
        return self.nombres

    def get_absolute_url(self):
        return reverse('registro:editar_beneficiarios', kwargs={'pk': self.pk})
