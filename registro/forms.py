# -*- coding: utf-8 -*-
from django import forms
from registro.models import Persona, Direccion
from django.forms import (
    TextInput, CharField, Select, RadioSelect, Textarea, CheckboxInput, DateTimeField
)
from datetime import *
from django.contrib.admin.widgets import AdminDateWidget

#date=datetime.now
date=datetime.today().strftime('%Y-%m-%d')

ESTATUS = (
    ('', 'Seleccione'),
    ('a', 'Aprobado'),
    ('e', 'En proceso'),
    ('n', 'Negado')
)

ESTADOS = (
    ('Mérida', 'Mérida'),
)

MUNICIPIOS = (
    ('', 'Seleccione'),
    ('Alberto Adriani','Alberto Adriani'),
    ('Andrés Bello','Andrés Bello'),
    ('Antonio Pinto Salinas','Antonio Pinto Salinas'),
    ('Aricagua','Aricagua'),
    ('Arzobispo Chacón','Arzobispo Chacón'),
    ('Campo Elías','Campo Elías'),
    ('Caracciolo Parra Olmedo','Caracciolo Parra Olmedo'),
    ('Cardenal Quintero','Cardenal Quintero'),
    ('Guaraque','Guaraque'),
    ('Julio César Salas','Julio César Salas'),
    ('Justo Briceño','Justo Briceño'),
    ('Libertador','Libertador'),
    ('Miranda','Miranda'),
    ('Obispo Ramos de Lora','Obispo Ramos de Lora'),
    ('Padre Noguera','Padre Noguera'),
    ('Pueblo Llano','Pueblo Llano'),
    ('Rangel','Rangel'),
    ('Rivas Dávila','Rivas Dávila'),
    ('Santos Marquina','Santos Marquina'),
    ('Sucre','Sucre'),
    ('Tovar','Tovar'),
    ('Tulio Febres Cordero','Tulio Febres Cordero'),
    ('Zea','Zea'),
)

PARROQUIAS = (
    ('', 'Seleccione'),
    ('Presidente Betancourt','Alberto Adriani - Presidente Betancourt'),
    ('Presidente Páez','Alberto Adriani - Presidente Páez'),
    ('Presidente Rómulo Gallegos','Alberto Adriani - Presidente Rómulo Gallegos'),
    ('Gabriel Picón González','Alberto Adriani - Gabriel Picón González'),
    ('Héctor Amable Mora','Alberto Adriani - Héctor Amable Mora'),
    ('José Nucete Sardi','Alberto Adriani - José Nucete Sardi'),
    ('Pulido Méndez','Alberto Adriani - Pulido Méndez'),
    ('Capital Andrés Bello','Andrés Bello - Capital Andrés Bello'),
    ('Capital Antonio Pinto Salinas','Antonio Pinto Salinas - Capital Antonio Pinto Salinas'),
    ('Mesa Bolívar','Antonio Pinto Salinas - Mesa Bolívar'),
    ('Mesa de Las Palmas','Antonio Pinto Salinas - Mesa de Las Palmas'),
    ('Capital Aricagua','Aricagua - Capital Aricagua'),
    ('San Antonio','Aricagua - San Antonio'),
    ('Capital Arzobispo Chacón','Arzobispo Chacón - Capital Arzobispo Chacón'),
    ('Capurí','Arzobispo Chacón - Capurí'),
    ('Chacantá','Arzobispo Chacón - Chacantá'),
    ('El Molino','Arzobispo Chacón - El Molino'),
    ('Guaimaral','Arzobispo Chacón - Guaimaral'),
    ('Mucutuy','Arzobispo Chacón - Mucutuy'),
    ('Mucuchachí','Arzobispo Chacón - Mucuchachí'),
    ('Fernández Peña','Campo Elías - Fernández Peña'),
    ('Matriz','Campo Elías - Matriz'),
    ('Montalbán','Campo Elías - Montalbán'),
    ('Acequias','Campo Elías - Acequias'),
    ('Jají','Campo Elías - Jají'),
    ('La Mesa','Campo Elías - La Mesa'),
    ('San José del Sur','Campo Elías - San José del Sur'),
    ('Capital Caracciolo Parra Olmedo','Caracciolo Parra Olmedo - Capital Caracciolo Parra Olmedo'),
    ('Florencio Ramírez','Caracciolo Parra Olmedo - Florencio Ramírez'),
    ('Capital Cardenal Quintero','Cardenal Quintero - Capital Cardenal Quintero'),
    ('Las Piedras','Cardenal Quintero - Las Piedras'),
    ('Capital Guaraque','Guaraque - Capital Guaraque'),
    ('Mesa de Quintero','Guaraque - Mesa de Quintero'),
    ('Río Negro','Guaraque - Río Negro'),
    ('Capital Julio César Salas','Julio César Salas - Capital Julio César Salas'),
    ('Palmira','Julio César Salas - Palmira'),
    ('Capital Justo Briceño','Justo Briceño - Capital Justo Briceño'),
    ('San Cristóbal de Torondoy','Justo Briceño - San Cristóbal de Torondoy'),
    ('Antonio Spinetti Dini','Libertador - Antonio Spinetti Dini'),
    ('Arias','Libertador - Arias'),
    ('Caracciolo Parra Pérez','Libertador - Caracciolo Parra Pérez'),
    ('Domingo Peña','Libertador - Domingo Peña'),
    ('El Llano','Libertador - El Llano'),
    ('Gonzalo Picón Febres','Libertador - Gonzalo Picón Febres'),
    ('Jacinto Plaza','Libertador - Jacinto Plaza'),
    ('Juan Rodríguez Suárez','Libertador - Juan Rodríguez Suárez'),
    ('Lasso de la Vega','Libertador - Lasso de la Vega'),
    ('Mariano Picón Salas','Libertador - Mariano Picón Salas'),
    ('Milla','Libertador - Milla'),
    ('Osuna Rodríguez','Libertador - Osuna Rodríguez'),
    ('Sagrario','Libertador - Sagrario'),
    ('El Morro','Libertador - El Morro'),
    ('Los Nevados','Libertador - Los Nevados'),
    ('Capital Miranda','Miranda - Capital Miranda'),
    ('Andrés Eloy Blanco','Miranda - Andrés Eloy Blanco'),
    ('La Venta','Miranda - La Venta'),
    ('Piñango','Miranda - Piñango'),
    ('Capital Obispo Ramos de Lora','Obispo Ramos de Lora - Capital Obispo Ramos de Lora'),
    ('Eloy Paredes','Obispo Ramos de Lora - Eloy Paredes'),
    ('San Rafael de Alcázar','Obispo Ramos de Lora - San Rafael de Alcázar'),
    ('Capital Padre Noguera','Padre Noguera - Capital Padre Noguera'),
    ('Capital Pueblo Llano','Pueblo Llano - Capital Pueblo Llano'),
    ('Capital Rangel','Rangel - Capital Rangel'),
    ('Cacute','Rangel - Cacute'),
    ('La Toma','Rangel - La Toma'),
    ('Mucurubá','Rangel - Mucurubá'),
    ('San Rafael','Rangel - San Rafael'),
    ('Capital Rivas Dávila','Rivas Dávila - Capital Rivas Dávila'),
    ('Gerónimo Maldonado','Rivas Dávila - Gerónimo Maldonado'),
    ('Capital Santos Marquina','Santos Marquina - Capital Santos Marquina'),
    ('Capital Sucre','Sucre - Capital Sucre'),
    ('Chiguará','Sucre - Chiguará'),
    ('Estánquez','Sucre - Estánquez'),
    ('La Trampa','Sucre - La Trampa'),
    ('Pueblo Nuevo del Sur','Sucre - Pueblo Nuevo del Sur'),
    ('San Juan','Sucre - San Juan'),
    ('El Amparo','Tovar - El Amparo'),
    ('El Llano','Tovar - El Llano'),
    ('San Francisco','Tovar - San Francisco'),
    ('Tovar','Tovar - Tovar'),
    ('Capital Tulio Febres Cordero','Tulio Febres Cordero - Capital Tulio Febres Cordero'),
    ('Independencia','Tulio Febres Cordero - Independencia'),
    ('María de la Concepción Palacios Blanco','Tulio Febres Cordero - María de la Concepción Palacios Blanco'),
    ('Santa Apolonia','Tulio Febres Cordero - Santa Apolonia'),
    ('Capital Zea','Zea - Capital Zea'),
    ('Caño El Tigre','Zea - Caño El Tigre'),
)

class PersonaForm(forms.ModelForm):

    codigo = forms.CharField(label="Código", widget=TextInput(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
            #'value':'001',
        }), required = True)

    nombres = forms.CharField(label="Nombres", widget=TextInput(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
            #'value':'Maria Gertrudiz',
        }), required = True)

    apellidos = forms.CharField(label="Apellidos", widget=TextInput(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
            #'value':'Parra Pérez',
        }), required = True)

    cedula = forms.CharField(label="Cédula", widget=TextInput(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
            #'value':'18902230',
        }), required = True)

    telefono = forms.CharField(label="Número telefónico", widget=TextInput(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
            #'value':'04263315899',
        }), required = True)

    estado = forms.ChoiceField(label="Estado", widget=Select(attrs={
            'class':'form-control input-md',
            #'style': 'min-width: 0; width: 100%; display: inline;',
        }), choices = ESTADOS)

    municipio = forms.ChoiceField(label="Municipio", widget=Select(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
        }), choices = MUNICIPIOS)

    parroquia = forms.ChoiceField(label="Parroquia", widget=Select(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
        }), choices = PARROQUIAS)

    '''
    direccion = forms.ChoiceField(label="direccion", widget=Select(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
    }), choices=Direccion.objects.all().values_list('id','nombre_direccion'))
    '''

    observacion = forms.CharField(label="Observación", widget=Textarea(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; height: 100px; display: inline;',
      }), required = True)

    enviado_a = forms.CharField(label="Enviado a", widget=TextInput(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
            #'value':'David Webb',
    }), required = True)

    estatus = forms.ChoiceField(label="Estatus", widget=Select(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
    }), choices = ESTATUS)

    fecha_solicitud = forms.DateField(initial=datetime.now, widget= AdminDateWidget(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
            #'disabled': 'disabled',
            'readonly': 'True',
    }), required = True)

    '''
    fecha_respuesta = forms.CharField(label="Fecha de respuesta", widget=TextInput(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
            'value':'0000-00-00',
    }),required = False)
    '''

    class Meta:

        model = Persona
        fields = '__all__'


class DireccionForm(forms.ModelForm):

    nombre_direccion = forms.CharField(label="Nombre de la Dirección", widget=TextInput(attrs={
            'class':'form-control input-md',
            'style': 'min-width: 0; width: 100%; display: inline;',
    }), required = True)

    class Meta:

        model = Direccion
        fields = '__all__'
