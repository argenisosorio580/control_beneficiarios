/**
 * @brief Función que desabilita la edición del campo de fecha_solicitud.
 */
$("document").ready(function () {
  //$("#id_fecha_solicitud").prop('disabled', true);
});

/**
 * @brief Función que hace desaparecer las notificaciones(alerts) de bootstrap.
 */
$("document").ready(function () {
  $(".alert-success").fadeTo(3000, 800).slideUp(50, function(){
    $(".alert-success").effect('blind');
  });
});

/*
Funciones para cargar el datepicker jquery en los campos de fecha.
*/
$(function() {
  /*
  $("#id_fecha_solicitud").datepicker ({
      //dateFormat: "yy-mm-dd",
      dateFormat: "dd/mm/yy",
      dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],
      dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
      firstDay: 1,
      gotoCurrent: true,
      monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]
    }
  );
  */
  $("#id_fecha_respuesta").datepicker ({
      //dateFormat: "yy-mm-dd",
      dateFormat: "dd/mm/yy",
      dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],
      dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
      firstDay: 1,
      gotoCurrent: true,
      monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]
    }
  );
});

/*
Funcion que carga los métodos de DataTables sobre la tabla especificada.
*/
$(document).ready(function() {
  var t = $('#example').DataTable({
    /* Poner la tabla en español */
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "No hay datos",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoEmpty": "No records available",
      "infoFiltered": "(filtered from _MAX_ total records)",
      "search": "Buscar",
      "paginate": {
        "first": "Primero",
        "last": "Último",
        "next": "Siguiente",
        "previous": "Anterior"
      },
    },
    /* Poner la columna index */
    "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "order": [[ 1, 'asc' ]]
      });
      t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
        });
      }).draw();
});
